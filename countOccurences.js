function countOccurences(strValue) {

  var strArray = strValue.split('');

  var uniqueLetters = {};

  strArray.forEach(function(letter){
      if(uniqueLetters[letter]){
        uniqueLetters[letter]++;
      }else{
        uniqueLetters[letter] = 1;
      }
  });

  console.log(uniqueLetters);
  return uniqueLetters;
}
